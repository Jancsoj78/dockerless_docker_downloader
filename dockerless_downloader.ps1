#region: Workaround for SelfSigned Cert an force TLS 1.2
add-type @”
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
public bool CheckValidationResult(
ServicePoint srvPoint, X509Certificate certificate,
WebRequest request, int certificateProblem) {
return true;
}
}
“@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
[System.Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
#endregion
#https://blog.ukotic.net/2017/08/15/could-not-establish-trust-relationship-for-the-ssltls-invoke-webrequest/

#powershell dockerless docker-image downloader on limited networks
#2019.03

#houskeeping
rm *.gz

$image = "ubuntu"
#kibana
$tag = "latest"
# 5
$imageuri = "https://auth.docker.io/token?service=registry.docker.io&scope=repository:library/"+$image+":pull"
$taguri = "https://registry-1.docker.io/v2/library/"+$image+"/manifests/"+$tag
$bloburi = "https://registry-1.docker.io/v2/library/"+$image+"/blobs/sha256:"

#ntlm auth

#$browser = New-Object System.Net.WebClient
#$browser.Proxy.Credentials =[System.Net.CredentialCache]::DefaultNetworkCredentials

#token request
$token = Invoke-WebRequest -Uri $imageuri | ConvertFrom-Json | Select -expand token

echo token: $token

#pull image manifest
$blobs = $($(Invoke-Webrequest -Headers @{Authorization="Bearer $token"} -Method GET -Uri $taguri | ConvertFrom-Json | Select -expand fsLayers ) -replace "sha256:" -replace "@{blobSum=" -replace "}")

#download blobs
for ($i=0; $i -lt $blobs.length; $i++) {
    $blobelement =$blobs[$i]
   
    Invoke-Webrequest -Headers @{Authorization="Bearer $token"} -Method GET -Uri $bloburi$blobelement -OutFile blobtmp
    
    $source = "blobtmp"
    $newfile = "$blobelement.gz"

#bug : not overwrite
#Rename-Item $source -NewName $newfile -force

#overwrite
Copy-Item $source $newfile -Force -Recurse
#source blobs
ls *.gz

}
#postprocess
echo "copy these .gz to your docker machine"
echo "docker import .gz backward"
echo "lastone with ubuntu:latest"
echo "after docker export and reinport to make a simple layer image"